#### source http://vision.stanford.edu/cs598_spring07/papers/Lecun98.pdf
#### source https://github.com/jmlee8939/LeNet-5_1998_pytorch

import torch.nn.functional as F
from torch.nn.parameter import Parameter

import torch
import torch.nn as nn
import math
import cv2


class Tanh(nn.Module):
    def forward(self, x):
        return 1.7159 * torch.tanh(x * 2 / 3)


class C1(nn.Module):
    def __init__(self):
        super(C1, self).__init__()
        self.c1 = nn.Conv2d(in_channels=1, out_channels=6, kernel_size=(5, 5), stride=(1, 1))
        self.reset_parameters()

    def forward(self, x):
        x = self.c1(x)
        return x

    def reset_parameters(self):
        nn.init.kaiming_uniform_(self.c1.weight, a=math.sqrt(2))
        self.c1.bias.data.fill_(0.01)


class S2(nn.Module):
    def __init__(self):
        super(S2, self).__init__()
        self.kernel_size = 2
        self.weight = Parameter(torch.Tensor(1, 6, 1, 1))
        self.bias = Parameter(torch.Tensor(1, 6, 1, 1))
        self.reset_parameters()

    def forward(self, x):
        x = F.avg_pool2d(x, self.kernel_size)
        x = x * self.weight + self.bias
        return x

    def reset_parameters(self):
        nn.init.kaiming_uniform_(self.weight, a=math.sqrt(1))
        self.bias.data.fill_(0.01)


class C3(nn.Module):
    def __init__(self):
        super(C3, self).__init__()
        self.weight = Parameter(torch.Tensor(10, 6, 5, 5))
        self.bias = Parameter(torch.Tensor(1, 16, 1, 1))
        self.kernel_size = 5
        self.out_channels = 16
        self.reset_parameters()

    def map_combine_list(self):
        connection_list = [[0, 4, 5, 6, 9, 10, 11, 12, 14, 15],
                           [0, 1, 5, 6, 7, 10, 11, 12, 13, 15],
                           [0, 1, 2, 6, 7, 8, 11, 13, 14, 15],
                           [1, 2, 3, 6, 7, 8, 9, 12, 14, 15],
                           [2, 3, 4, 7, 8, 9, 10, 12, 13, 15],
                           [3, 4, 5, 8, 9, 10, 11, 13, 14, 15]]
        return connection_list

    def forward(self, x):
        B_size = x.size(0)
        output_size = x.size(3) - self.kernel_size + 1
        output = torch.zeros(B_size, self.out_channels, output_size, output_size)
        list_ = self.map_combine_list()
        for i in range(len(list_)):
            output[:, list_[i], :, :] += (F.conv2d(x[:, i, :, :].unsqueeze(1),
                                                   self.weight[:, i, :, :].unsqueeze(1)) + self.bias[:, list_[i], :, :])
        return output

    def reset_parameters(self):
        nn.init.kaiming_uniform_(self.weight, a=math.sqrt(2))
        self.bias.data.fill_(0.01)


class S4(nn.Module):
    def __init__(self):
        super(S4, self).__init__()
        self.kernel_size = 2
        self.weight = Parameter(torch.Tensor(1, 16, 1, 1))
        self.bias = Parameter(torch.Tensor(1, 16, 1, 1))
        self.reset_parameters()

    def forward(self, x):
        x = F.avg_pool2d(x, self.kernel_size)
        x = x * self.weight + self.bias
        return x

    def reset_parameters(self):
        nn.init.kaiming_uniform_(self.weight, a=math.sqrt(1))
        self.bias.data.fill_(0.01)


class C5(nn.Module):
    def __init__(self):
        super(C5, self).__init__()
        self.c5 = nn.Conv2d(in_channels=16, out_channels=120, kernel_size=(5, 5), stride=(1, 1))
        self.reset_parameters()

    def forward(self, x):
        x = self.c5(x)
        return x

    def reset_parameters(self):
        nn.init.kaiming_uniform_(self.c5.weight, a=math.sqrt(2))
        self.c5.bias.data.fill_(0.01)


class RBF(nn.Module):
    def __init__(self):
        super(RBF, self).__init__()
        self.in_features = 84
        self.out_features = 10
        self.kernel = self.rbf_tensor()

    def forward(self, x):
        size = (x.size(0), self.out_features, self.in_features)
        x = x.unsqueeze(1).expand(size)
        c = self.kernel.unsqueeze(0).expand(size)
        output = (x - c).pow(2).sum(-1)
        return output

    def rbf_tensor(self):
        kernel_list = []
        for i in range(10):
            file = './RBF_kernel/' + str(i) + '_RBF.jpg'
            image = cv2.imread(file, 0)
            image = cv2.threshold(image, 127, 1, cv2.THRESH_BINARY)[1] * -1 + 1
            kernel_list.append(image.flatten())
        return (torch.Tensor(kernel_list))


class LeNet5(nn.Module):

    def __init__(self):
        super(LeNet5, self).__init__()

        self.feature_extractor = nn.Sequential(
            C1(),
            Tanh(),

            S2(),
            Tanh(),

            C3(),
            Tanh(),

            S4(),
            Tanh(),

            C5(),
            Tanh()
        )

        self.classifier = nn.Sequential(
            nn.Linear(in_features=120, out_features=84),
            Tanh(),
            RBF(),
        )

    def forward(self, x):
        x = self.feature_extractor(x)
        x = torch.flatten(x, 1)
        x = self.classifier(x)
        return x
